package com.example.weekeighttask

import android.view.View

interface ClickHandler {
    fun onButtonClick(view : View)
}