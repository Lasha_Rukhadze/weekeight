package com.example.weekeighttask

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weekeighttask.databinding.ButtonItemBinding
import kotlin.properties.Delegates

class RecyclerViewAdapter(val clickHandler: ClickHandler) : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    private val number = mutableListOf<Int>()
    private var startNumber : Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ButtonItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return number.size
    }

    inner class ItemViewHolder(private val binding: ButtonItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private var model by Delegates.notNull<Int>()
        fun bind(){
            settter()
            model = number[adapterPosition]
            binding.numberButton.text = model.toString()
            binding.numberButton.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            clickHandler.onButtonClick()
        }
    }

    fun settter(){
        number.add(++startNumber)
    }

}